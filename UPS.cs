using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamarin.Forms;
using TILL.POS.Model;
using TILL.POS.PrinterInterface;
using System.Net;
using Acr.UserDialogs;
using System.Net.NetworkInformation;
using Plugin.Connectivity;
using System.Threading.Tasks;
using static TILL.Domain.Constants.Enums;
using TILL.Domain.Constants;

namespace TILL.POS.Helpers
{
    public static class UniversalPrinterService
    {
        private static string PRINTER_NAME = "sdfdsf";

        #region KOT Printing

        public static void PrintKots(LocalOrder currentOrder)
        {
            var printers = App.LocalDal.GetData<LocalSettings>().Printers;

            printers = printers.Where(x => App.CurrentRegister.Printers.Contains(x.Id)).ToList();

            if (App.CurrentRegister.PrintReceipt)
            {
                foreach (var printer in printers)
                {
                    if ((printer.Mode == Domain.Constants.Enums.PrintMode.Both.ToString() || printer.Mode == Domain.Constants.Enums.PrintMode.Kitchen.ToString()) && !string.IsNullOrEmpty(printer.ConnectionName))
                    {
                        var filteredOrderItems = currentOrder.OrderItems.Where(oi => printer.Products.Contains((oi.VariantProduct ?? oi.Product).Id));

                        if (filteredOrderItems.Count() == 0) continue;

                        Reset();

                        if (currentOrder.OrderStatus == Domain.Constants.Enums.OrderStatus.Cancelled)
                        {
                            PrintKotHeader(currentOrder, true);
                            PrintCancelledKotOrderItems(filteredOrderItems);
                            PrintKotFooter(currentOrder);
                            Flush(printer);
                        }
                        else
                        {
                            if (filteredOrderItems.Any(x => x.OrderItemStatus == Domain.Constants.Enums.OrderItemStatus.New || x.OrderItemStatus == Domain.Constants.Enums.OrderItemStatus.Edited))
                            {
                                PrintKotHeader(currentOrder);
                                PrintKotOrderItems(filteredOrderItems);
                                PrintKotFooter(currentOrder);
                                Flush(printer);
                            }
                            else if(currentOrder.KotLogs.Any(x => !x.IsPrinted))
                            {
                                PrintKotHeader(currentOrder);
                                PrintKotFooter(currentOrder);
                                Flush(printer);
                            }
                        }
                    }
                }
            }
        }

        public static void PrintCancelledKot(LocalOrder currentOrder)
        {
            var printers = App.LocalDal.GetData<LocalSettings>().Printers;

            printers = printers.Where(x => App.CurrentRegister.Printers.Contains(x.Id)).ToList();

            if (App.CurrentRegister.PrintReceipt)
            {
                foreach (var printer in printers)
                {
                    var printerMode = printer.Mode;
                    if ((printerMode == (Domain.Constants.Enums.PrintMode.Both).ToString() || printerMode == (Domain.Constants.Enums.PrintMode.Kitchen).ToString()) && !string.IsNullOrEmpty(printer.ConnectionName))
                    {
                        var filteredOrderItems = currentOrder.OrderItems.Where(oi => printer.Products.Contains((oi.VariantProduct ?? oi.Product).Id));

                        if (filteredOrderItems.Count() == 0) continue;

                        Reset();

                        PrintKotHeader(currentOrder);

                        PrintCancelledKotOrderItems(filteredOrderItems);

                        PrintKotFooter(currentOrder);

                        Flush(printer);
                    }
                }
            }
        }

        private static void PrintKotHeader(LocalOrder currentOrder, bool IsCancelled = false)
        {
            // Adding the printer name
            AppendLines(MessageStyle.AlignCenter, Helpers.Settings.BusinessName);
            AppendEmptyLines();

            #region First Line

            Append("Order # ");
            AppendWithAlignment(MessageStyle.Bold, MessageAlignment.Left, currentOrder.OrderNumber, 14);

            AppendWithAlignment(MessageStyle.Bold, MessageAlignment.Left, string.Format("{0} {1}", DateTime.Now.ToString("dd/MM/yyyy"), DateTime.Now.ToString("h:mm:ss tt")), 20);
            AppendEmptyLines();

            #endregion

            #region Second line

            Append("by ");
            Append(MessageStyle.Underline, currentOrder.OrderPlacementDetail.StaffName.ToUpper());

            if (!string.IsNullOrEmpty(currentOrder.OrderPlacementDetail.RegisterName))
            {
                Append("    from ");
                Append(MessageStyle.Underline, currentOrder.OrderPlacementDetail.RegisterName.ToUpper());
            }

            AppendEmptyLines();
            AppendEmptyLines();

            #endregion

            #region Third Line

            if (IsCancelled)
            {
                AppendLines(MessageStyle.AlignCenter | MessageStyle.SizeDouble | MessageStyle.Bold, "CANCELLED\n");
            }

            Append(MessageStyle.Bold, currentOrder.OrderType.Name.ToUpper());

            if (currentOrder.OrderType.Name.Equals("dine in", StringComparison.OrdinalIgnoreCase) && currentOrder.Table != null)
            {
                AppendSpaces(3);
                Append(MessageStyle.Bold, currentOrder.Table.Code.ToUpper());
            }

            if (!IsCancelled)
            {
                AppendSpaces(3);
                Append(MessageStyle.Bold, currentOrder.OrderActions.Count == 1 ? "NEW ORDER" : "EDITED");

                if (currentOrder.Table != null)
                {
                    AppendSpaces(3);
                    Append(MessageStyle.Bold | MessageStyle.AlignRight, currentOrder.Table.GuestCount); //🙍
                    Append(" Guests");
                }
            }

            AppendEmptyLines();

            #endregion

            AppendHorizontalLine();
        }

        private static void PrintCancelledKotOrderItems(IEnumerable<LocalOrderItem> orderItems)
        {
            var sortedOrderItems = orderItems.OrderBy(o => new Tuple<string, string>(o.Product.CategoryName, o.Product.Name)).ToArray();

            var rows = new List<Tuple<string, object, object>>();

            string lastCategoryName = "";

            int WidthColumn1 = 10,
                WidthColumn2 = 5,
                WidthColumn3 = 28;

            AppendLines(MessageStyle.AlignLeft | MessageStyle.Bold, "CANCELLED ITEMS\n");
            AppendEmptyLines(1);


            foreach (var orderItem in orderItems.Where(x => x.OrderItemStatus == Domain.Constants.Enums.OrderItemStatus.Cancelled))
            {
                var kotItem = orderItem.KotLog.Last();

                if (!kotItem.IsPrinted)
                {
                    AppendTuppleLine(Tuple.Create(MessageAlignment.Right, MessageAlignment.Center, MessageAlignment.Left), new Tuple<string, object, object>(kotItem.Qty + "", "x", kotItem.Name), Tuple.Create(WidthColumn1, WidthColumn2, WidthColumn3));

                    if (kotItem.ModifierKotLog != null)
                    {
                        foreach (var modifier in kotItem.ModifierKotLog)
                        {
                            AppendTuppleLine(Tuple.Create(MessageAlignment.Right, MessageAlignment.Left), new Tuple<string, object>(modifier.IsDeleted ? "-" : "+", $"  + {modifier.Qty} * {modifier.Name}"), Tuple.Create(WidthColumn1 + WidthColumn2, WidthColumn3));
                            AppendEmptyLines(1);
                        }

                        if (!string.IsNullOrEmpty(kotItem.Notes))
                        {
                            AppendTuppleLine(MessageStyle.Bold, Tuple.Create(MessageAlignment.Right, MessageAlignment.Left, MessageAlignment.Left), new Tuple<string, object, object>("Item notes", "", $"[{kotItem.Notes}]"), Tuple.Create(WidthColumn1 + WidthColumn2 - 2, 2, WidthColumn3));
                            AppendEmptyLines(1);
                        }
                    }
                }
                kotItem.IsPrinted = true;

            }


        }


        private static void PrintKotOrderItems(IEnumerable<LocalOrderItem> orderItems)
        {
            var sortedOrderItems = orderItems.OrderBy(o => new Tuple<string, string>(o.Product.CategoryName, o.Product.Name)).ToArray();

            var rows = new List<Tuple<string, object, object>>();

            string lastCategoryName = "";

            int WidthColumn1 = 10,
                WidthColumn2 = 5,
                WidthColumn3 = 28;

            foreach (var orderItem in orderItems.Where(x => x.OrderItemStatus == Domain.Constants.Enums.OrderItemStatus.New || x.OrderItemStatus == Domain.Constants.Enums.OrderItemStatus.Edited))
            {
                var kotItem = orderItem.KotLog.Last();

                if (!kotItem.IsPrinted)
                {
                    if (!lastCategoryName.Equals(orderItem.Product.CategoryName))
                    {
                        lastCategoryName = orderItem.Product.CategoryName;
                        AppendLines(MessageStyle.Underline, orderItem.Product.CategoryName);
                        AppendEmptyLines(1);
                    }


                    if (kotItem.IsDeleted)
                    {
                        AppendLines(MessageStyle.AlignLeft | MessageStyle.Normal, "CANCELLED ITEM");
                    }

                    
                    AppendTuppleLine(Tuple.Create(MessageAlignment.Right, MessageAlignment.Center, MessageAlignment.Left), new Tuple<string, object, object>(kotItem.Qty == 0 ? "No Qty Change" : kotItem.Qty + "", "x", kotItem.Name), Tuple.Create(WidthColumn1, WidthColumn2, WidthColumn3));

                    if (kotItem.ModifierKotLog != null)
                    {
                        foreach (var modifier in kotItem.ModifierKotLog)
                        {
                            AppendTuppleLine(Tuple.Create(MessageAlignment.Right, MessageAlignment.Left), new Tuple<string, object>(modifier.IsDeleted ? "-" : "+", $" {modifier.Qty} * {modifier.Name}"), Tuple.Create(WidthColumn1 + WidthColumn2, WidthColumn3));
                            AppendEmptyLines(1);
                        }

                        if (!string.IsNullOrEmpty(kotItem.Notes))
                        {
                            AppendTuppleLine(MessageStyle.Bold, Tuple.Create(MessageAlignment.Right, MessageAlignment.Left, MessageAlignment.Left), new Tuple<string, object, object>("Item notes", "", $"[{kotItem.Notes}]"), Tuple.Create(WidthColumn1 + WidthColumn2 - 2, 2, WidthColumn3));
                            AppendEmptyLines(1);
                        }
                    }
                }
                kotItem.IsPrinted = true;
               
            }


        }

        private static void PrintKotFooter(LocalOrder currentOrder)
        {
            AppendHorizontalLine();
            var notes = currentOrder.KotLogs.Where(x => !x.IsPrinted);
            foreach(var note in notes)
            {
                if (!string.IsNullOrEmpty(note.Notes))
                {
                    AppendLines($"Order Notes: <b>{note.Notes}</b>");
                }
            }
  
            AppendLines(MessageStyle.AlignCenter | MessageStyle.Bold, $"KOT #: <b>{currentOrder.KotCount}</b>");
            AppendEmptyLines(5);
        }

        #endregion

        #region Billing Receipt

        public static void PrintBillingReceipt(LocalOrder order)
        {
            IPrinter devicePrinter = DependencyService.Get<IPrinter>();
            var printers = App.LocalDal.GetData<LocalSettings>().Printers;

            printers = printers.Where(x => App.CurrentRegister.Printers.Contains(x.Id)).ToList();

            if (App.CurrentRegister.PrintReceipt)
            {
                foreach (var printer in printers)
                {
                    var printerMode = printer.Mode;
                    if ((printerMode == Domain.Constants.Enums.PrintMode.Both.ToString() || printerMode == Domain.Constants.Enums.PrintMode.Billing.ToString()) && !string.IsNullOrEmpty(printer.ConnectionName))
                    {
                        Reset();

                        PrintBillingReceiptHeader(order);

                        PrintBillingOrderItems(order.OrderItems);

                        PrintBillingTotal(order);

                        if (order.OrderPayments != null)
                        {
                            PrintBillingPayments(order);
                        }
                        else
                        {
                            AppendLines(MessageStyle.AlignCenter | MessageStyle.Bold, "NOT PAID");
                            AppendHorizontalLine();
                        }

                        if (order.Customer != null)
                        {
                            AppendLines(MessageStyle.AlignCenter | MessageStyle.Bold, "CUSTOMER DETAILS");
                            AppendLines(MessageStyle.AlignCenter, order.Customer.Name, order.Customer.Phone, order.Customer.Address);
                            AppendHorizontalLine();
                        }

                        PrintReceiptFooter();

                        // TODO: Adjust the number of empty lines before cut command
                        AppendEmptyLines(5);

                        Flush(printer);
                    }
                }
            }
        }

        private static void PrintBillingReceiptHeader(LocalOrder order)
        {
            AppendLines(MessageStyle.AlignCenter | MessageStyle.Bold, Helpers.Settings.BusinessName, Helpers.Settings.OutletIdentifier, Helpers.Settings.OutletAddress, Helpers.Settings.OutletPhone);
            AppendHorizontalLine();

            List<Tuple<string, object, object>> rows = new List<Tuple<string, object, object>>() {
                new Tuple<string, object, object>("Receipt #", "", order.OrderNumber),
                new Tuple<string, object, object>("Served by", "", order.OrderPlacementDetail.StaffName),
                new Tuple<string, object, object>("Table #", "", order.Table!=null ? order.Table.Code : "N/A"),
                new Tuple<string, object, object>("Order type", "", order.OrderType.Name)
            };

            //AppendTuppleLines(rows, 18, 24);

            var table = new PrinterTableData
            {
                //Header = new Tuple<string, object, object>(order.StartTime.ToShortDateString(), "", "\b\b" + order.StartTime.ToShortTimeString()),
                Header = new Tuple<string, object, object>(order.StartTime.ToString("yyyy-MM-dd"), string.Empty, DateTime.Now.ToString("h:mm:ss tt")),
                StyleHeader = MessageStyle.Bold,

                Rows = rows,

                AlignmentColumn1 = MessageAlignment.Left,
                AlignmentColumn2 = MessageAlignment.Center,
                AlignmentColumn3 = MessageAlignment.Right,

                StyleColumn1 = MessageStyle.Bold,

                WidthColumn1 = 20,
                WidthColumn2 = 3,
                WidthColumn3 = 19
            };

            AppendTable(table);

            //foreach (var row in rows)
            //{
            //    AppendWithAlignment(MessageAlignment.Left, row.Item1, 18);
            //    AppendWithAlignment(MessageAlignment.Right, row.Item2, 24);
            //    AppendEmptyLines(1);
            //}

            AppendHorizontalLine();
        }

        private static void PrintBillingOrderItems(IEnumerable<LocalOrderItem> orderItems)
        {
            //AppendTuppleLine(MessageStyle.Underline | MessageStyle.Bold, new Tuple<string, object, object>("Item Name", "Qty", "Price"), 22, 8, 12);

            List<Tuple<string, object, object>> rows = new List<Tuple<string, object, object>>();

            foreach (var orderItem in orderItems.Where(x => x.OrderItemStatus != Domain.Constants.Enums.OrderItemStatus.Cancelled))
            {
                rows.Add(new Tuple<string, object, object>((orderItem.VariantProduct ?? orderItem.Product).Name, orderItem.QtyOrdered, orderItem.UnitPrice * orderItem.QtyOrdered));


                if (orderItem.Modifiers != null)
                {
                    foreach (var modifer in orderItem.Modifiers.Where(x => x.IsAdded || x.IsDeleted))
                    {
                        string type = " + ";
                        if (modifer.IsDeleted)
                        {
                            type = " - ";
                        }
                        rows.Add(new Tuple<string, object, object>(type + modifer.Name, modifer.Quantity, modifer.Quantity*modifer.UnitPrice));
                    }
                }
                if (orderItem.OrderItemDiscount != null)
                {
                    if (orderItem.OrderItemDiscount != null && orderItem.OrderItemDiscount.Amount > 0)
                    {
                        var disSymbol = orderItem.OrderItemDiscount.DiscountType.ToString() == "Amount" ? orderItem.DefaultCurrencySymbol : orderItem.OrderItemDiscount.Value+"%";
                        rows.Add(new Tuple<string, object, object>("Discount(" + disSymbol + ")", "", orderItem.OrderItemDiscount == null ? 0 : -orderItem.OrderItemDiscount.Amount));
                    }

                }
            }

            var table = new PrinterTableData
            {
                Header = new Tuple<string, object, object>("Item Name", "Qty", "Price"),
                StyleHeader = MessageStyle.Bold,

                Rows = rows,

                AlignmentColumn1 = MessageAlignment.Left,
                AlignmentColumn2 = MessageAlignment.Right,
                AlignmentColumn3 = MessageAlignment.Right,

                WidthColumn1 = 25,
                WidthColumn2 = 8,
                WidthColumn3 = 9
            };

            AppendTable(table);

            AppendHorizontalLine();
        }

        private static void PrintBillingTotal(LocalOrder order)
        {
            var rows = new List<Tuple<string, object, object>>();

            rows.Add(new Tuple<string, object, object>("Sub-total", order.Subtotal, ""));
            if (order.Discount != null && order.Discount.Amount > 0)
            {
                var disSymbol = order.Discount.DiscountType.ToString() == "Amount" ? order.DefaultCurrencySymbol : "%";
                rows.Add(new Tuple<string, object, object>("Discount("+ disSymbol+")", order.Discount == null ? 0 : order.Discount.Amount, ""));
            }
            rows.AddRange(order.OrderTaxes.Where(x => x.TaxType == Enums.TaxType.Tax).Select(ot => new Tuple<string, object, object>(ot.Name+"("+ot.Rate +"%)", ot.Amount, "")));
            rows.AddRange(order.OrderTaxes.Where(x => x.TaxType == Enums.TaxType.Fee).Select(ot => new Tuple<string, object, object>(ot.Name, ot.Amount, "")));

            rows.Add(new Tuple<string, object, object>(new string('-', 10), new string('-', 6), ""));
            rows.Add(new Tuple<string, object, object>("Total", order.Total, ""));


            var table = new PrinterTableData
            {
                Header = null,
                Rows = rows,

                WidthColumn1 = 22,
                WidthColumn2 = 20,

                AlignmentColumn1 = MessageAlignment.Right,
                AlignmentColumn2 = MessageAlignment.Right

            };

            AppendTable(table);

            AppendHorizontalLine();
        }

        private static void PrintBillingPayments(LocalOrder order)
        {
            var rows = new List<Tuple<string, object, object>>();

            rows.AddRange(order.OrderPayments.Select(op => new Tuple<string, object, object>(op.Name, op.Amount, "")));

            if (order.OrderPayments.Any(x => x.Name == "Cash"))
            {
                rows.Add(new Tuple<string, object, object>("Change Due", order.ChangeDue, ""));
            }

            var table = new PrinterTableData
            {
                Header = null,
                Rows = rows,

                WidthColumn1 = 22,
                WidthColumn2 = 20,

                AlignmentColumn1 = MessageAlignment.Right,
                AlignmentColumn2 = MessageAlignment.Right
            };

            AppendTable(table);
            AppendHorizontalLine();
        }

        private static void PrintReceiptFooter()
        {
            AppendLines(MessageStyle.AlignCenter, "=== THANK YOU ===");
            
        }

        #endregion

        #region Append Align (left, center, right) methods

        public static void AppendWithAlignment(MessageAlignment alignment, object msg, int width = 0)
        {
            AppendWithAlignment(MessageStyle.Normal, alignment, msg, width);
        }

        public static void AppendWithAlignment(MessageStyle style, MessageAlignment alignment, object msg, int width = 0)
        {
            msg = msg ?? "";

            if (alignment == DEFAULT_ALIGNEMNT)
            {
                Append(style, msg.ToString().PadRight(width));
            }
            else if (alignment == MessageAlignment.Center)
            {
                Append(style, PadBoth(msg.ToString(), width));
            }
            else if (alignment == MessageAlignment.Right)
            {
                Append(style, msg.ToString().PadLeft(width));
            }
        }

        public static void AppendRight(object msg, int width = 0)
        {
            Append(MessageStyle.Normal, msg.ToString().PadRight(width));
        }

        public static void AppendCenter(object msg, int width = 0)
        {
            Append(MessageStyle.Normal, PadBoth(msg.ToString(), width));
        }

        #endregion

        #region Append() methods

        public static void Append(params object[] msgs)
        {
            Append(MessageStyle.Normal, msgs);
        }

        public static void Append(MessageStyle style, params object[] msgs)
        {
            SetStyle(style);

            contentStream.Append(string.Join(" ", msgs));

            ResetStyle(style);
        }

        #endregion

        #region AppendLines() methods

        public static void AppendLines(params object[] msgs)
        {
            AppendLines(MessageStyle.Normal, msgs);
        }

        public static void AppendLines(MessageStyle style, params object[] msgs)
        {
            SetStyle(style);

            foreach (var msg in msgs)
            {
                if(msg != null)
                contentStream.AppendLine(msg.ToString());
            }

            ResetStyle(style);
        }

        #endregion

        #region AppendTupple() Methods

        public static void AppendTuppleLine(Tuple<string, object> tuple, Tuple<int, int> width)
        {
            AppendTuppleLine(MessageStyle.Normal
                , DEFAULT_ALIGNEMNT_TUPLE3
                , new Tuple<string, object, object>(tuple.Item1, tuple.Item2, "")
                , new Tuple<int, int, int>(width.Item1, width.Item2, 0)
                );
        }

        public static void AppendTuppleLine(Tuple<string, object, object> tuple, Tuple<int, int, int> width)
        {
            AppendTuppleLine(MessageStyle.Normal, DEFAULT_ALIGNEMNT_TUPLE3, tuple, width);
        }

        public static void AppendTuppleLine(Tuple<MessageAlignment, MessageAlignment> alignment, Tuple<string, object> tuple, Tuple<int, int> width)
        {
            AppendTuppleLine(MessageStyle.Normal
                , new Tuple<MessageAlignment, MessageAlignment, MessageAlignment>(alignment.Item1, alignment.Item2, DEFAULT_ALIGNEMNT)
                , new Tuple<string, object, object>(tuple.Item1, tuple.Item2, "")
                , new Tuple<int, int, int>(width.Item1, width.Item2, 0)
                );
        }

        public static void AppendTuppleLine(Tuple<MessageAlignment, MessageAlignment, MessageAlignment> alignment, Tuple<string, object, object> tuple, Tuple<int, int, int> width)
        {
            AppendTuppleLine(MessageStyle.Normal, alignment, tuple, width);
        }

        public static void AppendTuppleLine(MessageStyle messageStyle, Tuple<string, object> tuple, Tuple<int, int> width)
        {
            AppendTuppleLine(messageStyle
                , DEFAULT_ALIGNEMNT_TUPLE3
                , new Tuple<string, object, object>(tuple.Item1, tuple.Item2, "")
                , new Tuple<int, int, int>(width.Item1, width.Item2, 0)
                );
        }

        public static void AppendTuppleLine(MessageStyle messageStyle, Tuple<string, object, object> tuple, Tuple<int, int, int> width)
        {
            AppendTuppleLine(messageStyle, DEFAULT_ALIGNEMNT_TUPLE3, tuple, width);
        }

        public static void AppendTuppleLine(MessageStyle messageStyle, Tuple<MessageAlignment, MessageAlignment> alignment, Tuple<string, object> tuple, Tuple<int, int> width)
        {
            AppendTuppleLine(messageStyle
                , new Tuple<MessageAlignment, MessageAlignment, MessageAlignment>(alignment.Item1, alignment.Item2, DEFAULT_ALIGNEMNT)
                , new Tuple<string, object, object>(tuple.Item1, tuple.Item2, "")
                , new Tuple<int, int, int>(width.Item1, width.Item2, 0)
                );
        }

        public static void AppendTuppleLine(MessageStyle messageStyle, Tuple<MessageAlignment, MessageAlignment, MessageAlignment> alignment, Tuple<string, object, object> tuple, Tuple<int, int, int> width)
        {
            SetStyle(messageStyle);

            if (!string.IsNullOrEmpty(tuple.Item1))
            {
                AppendWithAlignment(alignment.Item1, tuple.Item1, width.Item1);
                AppendWithAlignment(alignment.Item2, tuple.Item2, width.Item2);
                AppendWithAlignment(alignment.Item3, tuple.Item3, width.Item3);
            }
            AppendEmptyLines(1);

            ResetStyle(messageStyle);
        }


        public static void AppendTuppleLines(IEnumerable<Tuple<string, object>> tuples, Tuple<int, int> width)
        {
            AppendTuppleLines(tuples.Select(t => new Tuple<string, object, object>(t.Item1, t.Item2, string.Empty)), new Tuple<int, int, int>(width.Item1, width.Item2, 0));
        }

        public static void AppendTuppleLines(IEnumerable<Tuple<string, object, object>> tuples, Tuple<int, int, int> width)
        {
            foreach (var tuple in tuples)
            {
                if (!string.IsNullOrEmpty(tuple.Item1))
                {
                    AppendWithAlignment(MessageAlignment.Left, tuple.Item1, width.Item1);
                    AppendWithAlignment(MessageAlignment.Left, tuple.Item2, width.Item2);
                    AppendWithAlignment(MessageAlignment.Left, tuple.Item3, width.Item3);
                }
                AppendEmptyLines(1);
            }
        }

        public static void AppendTable(PrinterTableData Table)
        {
            if (Table.Header != null)
            {
                AppendWithAlignment(Table.StyleHeader, Table.AlignmentColumn1, Table.Header.Item1, Table.WidthColumn1);
                AppendWithAlignment(Table.StyleHeader, Table.AlignmentColumn2, Table.Header.Item2, Table.WidthColumn2);
                AppendWithAlignment(Table.StyleHeader, Table.AlignmentColumn3, Table.Header.Item3, Table.WidthColumn3);
                AppendEmptyLines(1);
            }

            foreach (var row in Table.Rows)
            {
                if (!string.IsNullOrEmpty(row.Item1))
                {
                    AppendWithAlignment(Table.StyleColumn1, Table.AlignmentColumn1, row.Item1, Table.WidthColumn1);
                    AppendWithAlignment(Table.StyleColumn2, Table.AlignmentColumn2, row.Item2, Table.WidthColumn2);
                    AppendWithAlignment(Table.StyleColumn3, Table.AlignmentColumn3, row.Item3, Table.WidthColumn3);
                }
                AppendEmptyLines(1);
            }
        }

        #endregion

        #region Special Append methods

        public static void AppendSpaces(int count = 1)
        {
            AppendCenter(new string(' ', count));
        }

        public static void AppendHorizontalLine(char character = '-', int charCount = COUNT_CHAR_IN_HR_LINE)
        {
            AppendLines(MessageStyle.AlignCenter, new string(character, charCount));
        }

        public static void AppendEmptyLines(int count = 1)
        {
            for (int it = 0; it < count; it++)
            {
                AppendLines(string.Empty);
            }
        }

        public static void AppendAlphaLine()
        {
            contentStream.AppendLine();
            contentStream.AppendLine("[(--ABCDEFGHIJKLMNOPQRSTUVWXYZ--0123456789--)]");
        }

        public static void AppendImage(string imagePath)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Style methods

        public static void SetStyle(MessageStyle style)
        {
            SetStyles(BreakStyles(style));
        }

        private static void SetStyles(IEnumerable<MessageStyle> styles)
        {
            foreach (var style in styles)
            {
                SetSingleStyle(style);
            }
        }

        public static void SetSingleStyle(MessageStyle style)
        {
            string strStyle = string.Empty;

            if (COMMANDS.ContainsKey(style))
            {
                strStyle = ESC + COMMANDS[style] + COMMAND_ON_SUFFIX;
            }
            else
            {
                switch (style)
                {
                    case MessageStyle.SizeDouble:
                        strStyle = DOUBLE_ON;
                        break;
                    case MessageStyle.AlignLeft:
                        strStyle = ALIGN_LEFT;
                        break;
                    case MessageStyle.AlignCenter:
                        strStyle = ALIGN_CENTER;
                        break;
                    case MessageStyle.AlignRight:
                        strStyle = ALIGN_RIGHT;
                        break;
                }
            }

            contentStream.Append(strStyle);
        }

        public static void ResetStyle(MessageStyle style)
        {
            ResetStyles(BreakStyles(style));
        }

        private static void ResetStyles(IEnumerable<MessageStyle> styles)
        {
            foreach (var style in styles)
            {
                ResetSingleStyle(style);
            }
        }

        private static void ResetSingleStyle(MessageStyle style)
        {
            string strStyle = string.Empty;

            if (COMMANDS.ContainsKey(style))
            {
                strStyle = ESC + COMMANDS[style] + COMMAND_OFF_SUFFIX;
            }
            else
            {
                switch (style)
                {
                    case MessageStyle.SizeDouble:
                        strStyle = DOUBLE_OFF;
                        break;
                    case MessageStyle.AlignLeft:
                    case MessageStyle.AlignCenter:
                    case MessageStyle.AlignRight:
                        strStyle = ALIGN_LEFT;
                        break;
                }
            }

            contentStream.Append(strStyle);
        }

        public static void SetAlignment(MessageAlignment alignment = DEFAULT_ALIGNEMNT)
        {

        }

        private static IEnumerable<MessageStyle> BreakStyles(MessageStyle style)
        {
            IList<MessageStyle> styles = new List<MessageStyle>();

            if (style == MessageStyle.Normal)
            {
                return styles;
            }

            foreach (MessageStyle value in Enum.GetValues(style.GetType()))
            {
                if ((style & value) == value)
                {
                    styles.Add(value);
                }
            }
            return styles;
        }


        #endregion

        #region Special Print methods

        public static void Print(string msg, MessageStyle attrib, MessageAlignment alignment = DEFAULT_ALIGNEMNT)
        {
            throw new NotImplementedException();
        }

        public static void PrintLine(string msg, MessageStyle attrib, MessageAlignment alignment = DEFAULT_ALIGNEMNT)
        {
            throw new NotImplementedException();
        }

        public static void PrintTest()
        {
            Reset();

            AppendLines(MessageStyle.Normal, "Normal text");
            AppendLines(MessageStyle.Bold, "Bold text");
            AppendLines(MessageStyle.Underline, "Underline text");
            AppendLines(MessageStyle.DoubleStrike, "Double strike text");

            AppendLines(MessageStyle.AlignRight, "Right Align Text");
            AppendLines(MessageStyle.AlignCenter, "Center Align Text");
            AppendLines(MessageStyle.AlignLeft, "Left Align Text");

            AppendLines(MessageStyle.SizeDouble, "Size Double");

            AppendEmptyLines(1);

            Flush();
        }

        #endregion


        public static void Reset()
        {
            contentStream = new StringBuilder();
        }

        public static void Flush()
        {
            if (IPEndpoint == null)
            {
                throw new Exception("Defulat IPAddress of pritner not set");
            }
                Flush(IPEndpoint.Address.ToString(), IPEndpoint.Port);
            
        }

        public static bool IsPrinterReachableAndRunning(string ip)
        {
            var connectivity = CrossConnectivity.Current;
            if (!connectivity.IsConnected)
                return false;

            var reachable = connectivity.IsRemoteReachable(ip, DEFAULT_PRINTER_PORT,1000);

            return reachable.Result;
        }

        public static void Flush(Domain.DTOs.Outlet.PrinterDto printer)
        {
            Flush(printer.ConnectionName, DEFAULT_PRINTER_PORT);

        }

        public static void Flush(string ipAddress, int port)
        {
         //   if (IsPrinterReachableAndRunning(ipAddress))
          //  {
                var device = DependencyService.Get<IPrinter>();

                contentStream.Append(CMD_CUT);

                string content = contentStream.ToString()
                    .Replace("<b>", BOLD_ON)
                    .Replace("</b>", BOLD_OFF);


                try
                {
                    device.SendToPrinterAsync(ipAddress, port, content);
                }
                catch (Exception e)
                {
                    UserDialogs.Instance.Alert("Printer not found.");
                }
         //   }
        }


        #region PrinterCommands

        /// <summary>
        /// Null
        /// </summary>
        private static string NUL = Convert.ToString((char)0);

        /// <summary>
        /// Escape
        /// </summary>
        private static string ESC = Convert.ToString((char)27);

        /// <summary>
        /// Line Feed
        /// </summary>
        private static string LF = Convert.ToString((char)10);

        /// <summary>
        /// Group Separator
        /// </summary>
        private static string GS = Convert.ToString((char)29);

        private static string CMD_INITLIAZE_PRINTER = ESC + "@";

        private static string ALIGN_LEFT = ESC + "a" + Convert.ToString((char)0);
        private static string ALIGN_CENTER = ESC + "a" + Convert.ToString((char)1);
        private static string ALIGN_RIGHT = ESC + "a" + Convert.ToString((char)2);

        public static Dictionary<MessageStyle, string> COMMANDS = new Dictionary<MessageStyle, string>
        {
            {MessageStyle.Underline, "-" },
            {MessageStyle.Bold, "E" },
            {MessageStyle.DoubleStrike, "F" },
            {MessageStyle.Justification, "A" },
        };

        private static string COMMAND_ON_SUFFIX = "\u0001";
        private static string COMMAND_OFF_SUFFIX = "\0";

        private static string BOLD_ON = ESC + "E" + "\u0001";
        private static string BOLD_OFF = ESC + "E" + "\0";

        //private  string UNDERLINE_ON = ESC + "-" + "\u0001";
        //private  string UNDERLINE_OFF = ESC + "-" + "\0";

        private static string LINE_FEED = ESC + "d" + Convert.ToString((char)1);
        private static string FONT_B = ESC + "M" + Convert.ToString((char)2);

        private static string DOUBLE_ON = GS + "!" + "\u0011";
        private static string DOUBLE_OFF = GS + "!" + "\0";

        private static string CMD_CUT = $"{ESC}@{GS}V{(char)1}";
        private static string CMD_DRAWER = ESC + "@" + Convert.ToString((char)27) + "p" + Convert.ToString((char)0) + ".}";
        private static string CMD_OPEN_DRAWER = ESC + "p" + Convert.ToString((char)1);

        public static string CMD_INITLIAZE_PRINTER1 { get => CMD_INITLIAZE_PRINTER; set => CMD_INITLIAZE_PRINTER = value; }

        public enum MessageStyle
        {
            Normal = 0,
            Bold = 1 << 1,
            Underline = 1 << 2,
            DoubleStrike = 1 << 3,

            AlignLeft = 1 << 11,
            AlignCenter = 1 << 12,
            AlignRight = 1 << 13,
            Justification = 1 << 14,

            SizeDouble = 1 << 21,
        }

        public enum MessageAlignment
        {
            Left = 0,
            Center = 1,
            Right = 2
        }

        public enum PrinterCommand
        {
            OpenDrawer = 1,
            CutReceipt = 2
        }

        public class PrinterTableData
        {
            public Tuple<string, object, object> Header { get; set; }
            public IEnumerable<Tuple<string, object, object>> Rows { get; set; }

            public MessageStyle StyleHeader { get; set; } = MessageStyle.Normal;
            //public MessageStyle StyleRows { get; set; } = MessageStyle.Normal;

            public MessageAlignment AlignmentColumn1 { get; set; } = MessageAlignment.Left;
            public MessageAlignment AlignmentColumn2 { get; set; } = MessageAlignment.Left;
            public MessageAlignment AlignmentColumn3 { get; set; } = MessageAlignment.Left;

            public MessageStyle StyleColumn1 { get; set; } = MessageStyle.Normal;
            public MessageStyle StyleColumn2 { get; set; } = MessageStyle.Normal;
            public MessageStyle StyleColumn3 { get; set; } = MessageStyle.Normal;

            public int WidthColumn1 { get; set; } = 0;
            public int WidthColumn2 { get; set; } = 0;
            public int WidthColumn3 { get; set; } = 0;
        }

        #endregion

        public const MessageAlignment DEFAULT_ALIGNEMNT = MessageAlignment.Left;
        public static Tuple<MessageAlignment, MessageAlignment, MessageAlignment> DEFAULT_ALIGNEMNT_TUPLE3 = new Tuple<MessageAlignment, MessageAlignment, MessageAlignment>(DEFAULT_ALIGNEMNT, DEFAULT_ALIGNEMNT, DEFAULT_ALIGNEMNT);

        #region Protected Properties

        public static IPEndPoint IPEndpoint { get; set; }

        public static StringBuilder contentStream { get; set; }

        /// <summary>
        /// Count of characters in the horizontal line
        /// </summary>
        public const int COUNT_CHAR_IN_HR_LINE = 42; // 46
        public const int DEFAULT_PRINTER_PORT = 9100;

        #endregion


        public static string PadBoth(string str, int length)
        {
            int spaces = length - str.Length;
            int padLeft = spaces / 2 + str.Length;
            return str.PadLeft(padLeft).PadRight(length);
        }

    }

}


