import * as moment from "moment";
import DataTable from "../DataTable";
import Order from "../../interfaces/Order";
import OrderItem from "../../interfaces/OrderItem";
import Renderer from "../../interfaces/Renderer";
import PrinterStream from "../PrinterStream/index";
import { MessageStyle } from "../../enums/MessageStyle";
import { MessageAlignment } from "../../enums/MessageAlignment";
import * as pad from "pad";

export default class BillingRenderer implements Renderer {
  stream: PrinterStream;

  constructor() {
    this.stream = new PrinterStream();
  }

  public render(order: Order): string {
    this.renderHeader(order);
    this.renderOrderItems(order.orderItems);
    this.renderTotal(order);
    this.renderPayment(order);
    this.renderCustomer(order);
    this.renderFooter();

    // TODO: Adjust the number of empty lines before cut command
    this.stream.appendEmptyLines(5);

    return this.stream.getResult();
  }

  private renderHeader(order: Order) {
    const businessName: string = order.header.businessName;
    const gstin: string = order.header.businessId;
    const address: string = order.header.address;
    const phone: string = order.header.phone;

    this.stream.appendLines(MessageStyle.AlignCenter, businessName);
    this.stream.appendLines(MessageStyle.AlignCenter, gstin);
    this.stream.appendLines(MessageStyle.AlignCenter, address);
    this.stream.appendLines(MessageStyle.AlignCenter, phone);

    this.stream.appendHorizontalLine();

    const rows: Array<[string, string, string]> = [
      ["Receipt #", "", order.orderNumber],
      ["Served by", "", order.orderPlacementDetail.staffName],
      ["Table #", "", order.table ? order.table.code : "N/A"],
      ["Order type", "", order.orderType.name],
    ];

    const table: DataTable = {
      header: [moment(order.startTime).format("YYYY-MM-DD"), "", moment().format("hh:mm:ss A")],
      styleHeader: MessageStyle.Bold,

      rows: rows,

      alignmentColumn1: MessageAlignment.Left,
      alignmentColumn2: MessageAlignment.Center,
      alignmentColumn3: MessageAlignment.Right,

      styleColumn1: MessageStyle.Bold,
      styleColumn2: MessageStyle.Normal,
      styleColumn3: MessageStyle.Normal,

      widthColumn1: 20,
      widthColumn2: 3,
      widthColumn3: 19,
    };

    this.stream.appendTable(table);
    this.stream.appendHorizontalLine();
  }

  private renderFooter() {
    this.stream.appendLines(MessageStyle.AlignCenter, "=== THANK YOU ===");
  }

  private renderOrderItems(orderItems: Array<OrderItem>) {
    const rows: Array<[string, string, string]> = [];

    // orderItems.filter(x => x.orderItemStatus === "Cancelled").forEach(orderItem => {
    orderItems.forEach(orderItem => {
      rows.push([
        (orderItem.variantProduct ? orderItem.variantProduct : orderItem.product).name,
        `${orderItem.quantity}`,
        `${orderItem.price * orderItem.quantity}`,
      ]);

      if (orderItem.modifiers) {
        orderItem.modifiers.filter(x => x.isAdded || x.isDeleted).forEach(modifer => {
          let type: string = " + ";
          if (modifer.isDeleted) {
              type = " - ";
          }
          rows.push([type + modifer.name, `${modifer.quantity}`, `${modifer.quantity * modifer.price}`]);
        });
      }
      if (orderItem.orderItemDiscount) {
          if (orderItem.orderItemDiscount && orderItem.orderItemDiscount.amount > 0) {
            const disSymbol = orderItem.orderItemDiscount.discountType === "Amount" ? "$" : orderItem.orderItemDiscount.value + "%";
            rows.push(["Discount(" + disSymbol + ")", "", `${orderItem.orderItemDiscount ? 0 : -orderItem.orderItemDiscount.amount}`]);
          }

      }
    });

    const table: DataTable = {
      header: ["Item Name", "Qty", "Price"],
      styleHeader: MessageStyle.Bold,
      rows: rows,

      alignmentColumn1: MessageAlignment.Left,
      alignmentColumn2: MessageAlignment.Center,
      alignmentColumn3: MessageAlignment.Right,

      styleColumn1: MessageStyle.Normal,
      styleColumn2: MessageStyle.Normal,
      styleColumn3: MessageStyle.Normal,

      widthColumn1: 25,
      widthColumn2: 8,
      widthColumn3: 9,
    };

    this.stream.appendTable(table);

    this.stream.appendHorizontalLine();
  }

  private renderTotal(order: Order) {
    const rows: Array<[string, string, string]> = [];

    rows.push(["Sub-total", `${order.subtotal}`, ""]);

    if (order.discount && order.discount.amount) {
      const disSymbol = order.discount.discountType === "Amount" ? "$" : "%";
      rows.push([`Discount(${disSymbol})`, order.discount ? "0" : `${order.discount.amount}`, ""]);
    }

    // TODO: check groupTax
    order.orderTaxes.filter(x => x.taxType === "Tax").forEach(ot => {
      rows.push([`${ot.name} (${ot.rate} %)`, `${ot.amount}`, ""]);
    });

    order.orderTaxes.filter(x => x.taxType === "Fee").forEach(ot => {
      rows.push([`${ot.name}`, `${ot.amount}`, ""]);
    });

    rows.push([pad(10, "", "-"), pad(6, "", "-"), ""]);
    rows.push(["Total", `${order.total}`, ""]);

    const table: DataTable = {
      header: undefined,
      rows: rows,

      styleHeader: MessageStyle.Normal,

      widthColumn1: 22,
      widthColumn2: 20,
      widthColumn3: 0,

      alignmentColumn1: MessageAlignment.Right,
      alignmentColumn2: MessageAlignment.Right,
      alignmentColumn3: MessageAlignment.Right,

      styleColumn1: MessageStyle.Normal,
      styleColumn2: MessageStyle.Normal,
      styleColumn3: MessageStyle.Normal,

    };

    this.stream.appendTable(table);
    this.stream.appendHorizontalLine();
  }

  private renderCustomer(order: Order) {
    if (order.customer) {
        this.stream.appendLines(MessageStyle.AlignCenter | MessageStyle.Bold, "CUSTOMER DETAILS");
        this.stream.appendLines(MessageStyle.AlignCenter, order.customer.name, order.customer.phone, order.customer.address);
        this.stream.appendHorizontalLine();
    }
  }

  private renderPayment(order: Order) {
    if (order.orderPayments) {
      const rows: Array<[string, string, string]> = [];

      order.orderPayments.forEach(op => {
        rows.push([`${op.name}`, `${op.amount}`, ""]);
      });

      if (order.orderPayments.some(x => x.name === "Cash")) {
        rows.push(["Change Due", `${order.changeDue || 0}`, ""]);
      }

      const table: DataTable = {
        header: undefined,
        rows: rows,

        styleHeader: MessageStyle.Normal,

        widthColumn1: 22,
        widthColumn2: 20,
        widthColumn3: 0,

        alignmentColumn1: MessageAlignment.Right,
        alignmentColumn2: MessageAlignment.Right,
        alignmentColumn3: MessageAlignment.Right,

        styleColumn1: MessageStyle.Normal,
        styleColumn2: MessageStyle.Normal,
        styleColumn3: MessageStyle.Normal,
      };

      this.stream.appendTable(table);
      this.stream.appendHorizontalLine();
    } else {
      this.stream.appendLines(MessageStyle.AlignCenter | MessageStyle.Bold, "NOT PAID");
      this.stream.appendHorizontalLine();
    }
  }
}
