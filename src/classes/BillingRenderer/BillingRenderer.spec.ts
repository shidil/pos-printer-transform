import "mocha";
import * as order from "../../order.json";
import { expect } from "chai";
import BillingRenderer from "./index";

describe("BillingRenderer", () => {

  describe("constructor()", () => {

    it("should create a new empty BillingRenderer-object", () => {

      // ARRANGE

      // ACT
      const renderer = new BillingRenderer();
      const result = renderer.render(<any>order);

      console.log(result);

      // ASSERT
      expect(renderer).to.be.an("object");
      expect(result).to.be.a("string");
    });
  });
});
