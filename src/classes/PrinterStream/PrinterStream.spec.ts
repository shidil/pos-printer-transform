import "mocha";
import { expect } from "chai";
import PrinterStream from "./index";

describe("PrinterStream", () => {

  describe("constructor()", () => {

    it("should create a new empty PrinterStream-object", () => {

      // ARRANGE: Nothing to do here

      // ACT
      const printerStream = new PrinterStream();

      // ASSERT
      expect(printerStream).to.be.an("object");
      expect(printerStream.getResult()).to.be.eq("");
    });
  });
});
