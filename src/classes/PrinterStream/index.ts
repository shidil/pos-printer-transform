import * as pad from "pad";
import StringBuilder from "../StringBuilder";
import { MessageStyle } from "../../enums/MessageStyle";
import { MessageAlignment } from "../../enums/MessageAlignment";
import {
  COMMANDS,
  DOUBLE_ON,
  ALIGN_LEFT,
  ALIGN_RIGHT,
  ALIGN_CENTER,
  COMMAND_ON_SUFFIX,
  ESC,
  DEFAULT_ALIGNEMNT,
  DOUBLE_OFF,
  COMMAND_OFF_SUFFIX,
  COUNT_CHAR_IN_HR_LINE,
  BOLD_ON,
  BOLD_OFF} from "../../constants/transforms";
import DataTable from "../DataTable";

export default class PrinterStream {
  private contentStream: StringBuilder;

  constructor() {
    this.contentStream = new StringBuilder();

    console.log("Initialized data stream");
  }

  /**
   * Outputs the string stream
   * @returns {string} output stream as string
   */
  public getResult(): string {
    return this.contentStream.toString()
            .replace(/<b>/g, BOLD_ON)
            .replace(/<\/b>/g, BOLD_OFF);
  }

  public flush() {
    this.contentStream.clear();
  }

  public appendWithAlignment(style: MessageStyle = undefined, alignment: MessageAlignment, message: string = "", width: number = 0) {
    if (alignment === DEFAULT_ALIGNEMNT) {
      this.append(style, pad(message, width));
    } else if (alignment === MessageAlignment.Center) {
      this.append(style, this.padBoth(message, width));
    } else if (alignment === MessageAlignment.Right) {
      this.append(style, pad(width, message));
    }
  }

  public appendRight(message: string, width: number = 0) {
    this.append(MessageStyle.Normal, pad(width, message));
  }

  public appendCenter(message: string, width: number = 0) {
    this.append(MessageStyle.Normal, this.padBoth(message, width));
  }

  public append(style: MessageStyle, ...messages) {
    this.setStyle(style);

    this.contentStream.append(messages.join(" "));

    this.resetStyle(style);
  }

  public appendAlphaLine() {
    this.contentStream.appendLine(undefined);
    this.contentStream.appendLine("[(--ABCDEFGHIJKLMNOPQRSTUVWXYZ--0123456789--)]");
  }

  public appendEmptyLines(count: number = 1) {
      for (let it = 0; it < count; it++) {
        this.appendLines(MessageStyle.Normal, "");
      }
  }

  public appendSpaces(count: number = 1) {
    this.appendCenter(pad(count, ""));
  }

  public appendHorizontalLine(character: string = "-", charCount: number = COUNT_CHAR_IN_HR_LINE) {
    this.appendLines(MessageStyle.AlignCenter, pad(charCount, "", character));
  }

  public appendLines(style: MessageStyle, ...msgs) {
    this.setStyle(style);

    msgs.forEach(msg => {
      if (msg !== null && msg !== undefined) {
        this.contentStream.appendLine(msg.toString());
      }
    });

    this.resetStyle(style);
  }

  public appendTuppleLine(messageStyle: MessageStyle, alignment, tuple, width) {
    this.setStyle(messageStyle);

    if (tuple[0]) {
      this.appendWithAlignment(MessageStyle.Normal, alignment[0], tuple[0], width[0]);
      this.appendWithAlignment(MessageStyle.Normal, alignment[1], tuple[1], width[1]);
      this.appendWithAlignment(MessageStyle.Normal, alignment[2], tuple[2], width[2]);
    }
    this.appendEmptyLines(1);

    this.resetStyle(messageStyle);
  }

  public appendTuppleLines(tuples: Array<any>, width: [number, number, number]) {
    tuples.forEach(tuple => {
      if (tuple[0]) {
        this.appendWithAlignment(MessageStyle.Normal, MessageAlignment.Left, tuple[0], width[0]);
        this.appendWithAlignment(MessageStyle.Normal, MessageAlignment.Left, tuple[1], width[1]);
        this.appendWithAlignment(MessageStyle.Normal, MessageAlignment.Left, tuple[2], width[2]);
      }
      this.appendEmptyLines(1);
    });
  }

  public appendTable(table: DataTable) {
    if (table.header) {
        this.appendWithAlignment(table.styleHeader, table.alignmentColumn1, table.header[0], table.widthColumn1);
        this.appendWithAlignment(table.styleHeader, table.alignmentColumn2, table.header[1], table.widthColumn2);
        this.appendWithAlignment(table.styleHeader, table.alignmentColumn3, table.header[2], table.widthColumn3);
        this.appendEmptyLines(1);
    }

    table.rows.forEach(row => {
      if (row[0]) {
          this.appendWithAlignment(table.styleColumn1, table.alignmentColumn1, row[0], table.widthColumn1);
          this.appendWithAlignment(table.styleColumn2, table.alignmentColumn2, row[1], table.widthColumn2);
          this.appendWithAlignment(table.styleColumn3, table.alignmentColumn3, row[2], table.widthColumn3);
      }
      this.appendEmptyLines(1);
    });
  }

  public setStyle(style: MessageStyle) {
    this.setStyles(style);
  }

  public setStyles(...styles: Array<MessageStyle>) {
    styles.forEach(style => {
      this.setSingleStyle(style);
    });
  }

  public setSingleStyle(style: MessageStyle) {
    let strStyle: string = "";

    if (COMMANDS[style]) {
      strStyle = ESC + COMMANDS[style] + COMMAND_ON_SUFFIX;
    } else {
      switch (style) {
        case MessageStyle.SizeDouble:
            strStyle = DOUBLE_ON;
            break;
        case MessageStyle.AlignLeft:
            strStyle = ALIGN_LEFT;
            break;
        case MessageStyle.AlignCenter:
            strStyle = ALIGN_CENTER;
            break;
        case MessageStyle.AlignRight:
            strStyle = ALIGN_RIGHT;
            break;
        default: break;
      }
    }

    this.contentStream.append(strStyle);
  }

  public resetStyle(style: MessageStyle) {
    this.resetStyles(this.breakStyles(style));
  }

  public resetStyles(styles: Array<MessageStyle>) {
    styles.forEach(style => {
      this.resetSingleStyle(style);
    });
  }

  public resetSingleStyle(style: MessageStyle) {
    let strStyle: string = "";

    if (COMMANDS[style]) {
        strStyle = ESC + COMMANDS[style] + COMMAND_OFF_SUFFIX;
    } else {
      switch (style) {
          case MessageStyle.SizeDouble:
              strStyle = DOUBLE_OFF;
              break;
          case MessageStyle.AlignLeft:
          case MessageStyle.AlignCenter:
          case MessageStyle.AlignRight:
              strStyle = ALIGN_LEFT;
              break;
          default: break;
      }
    }

    this.contentStream.append(strStyle);
  }

  public breakStyles(style: MessageStyle): Array<MessageStyle> {
    let styles: Array<MessageStyle> = [];

    if (style === MessageStyle.Normal) {
      return styles;
    }

    styles.push(style);

    return styles;
  }

  public padBoth(str: string, length: number): string {
    const spaces: number = length - str.length;
    const padLeft: number = spaces / 2 + str.length;
    return pad(pad(padLeft, str), length);
  }
}
