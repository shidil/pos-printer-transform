/**
 * C# StringBuilder-like convenience object.
 */
export default class StringBuilder {
  public _newLine: string;
  private _position: number;
  private _ignoreNull: boolean;
  private _buffer: Array<string>;

  constructor(newLine: string = "\r\n", ignoreNull: boolean = true) {
    this._buffer = [];
    this._position = -1;
    this._newLine = newLine;
    this._ignoreNull = ignoreNull;
  }

  get length(): number {
    return this.toString().length;
  }

  append(text: string) {
    if (this._ignoreNull && (text === null || text === undefined)) {
        return;
    }
    this._buffer[++this._position] = text;
  }

  appendLine(text: string) {
    if (this._ignoreNull && (text === null || text === undefined)) {
        return;
    }

    this.append(text);
    this.append(this._newLine);
  }

  clear() {
    this._buffer = [];
    this._position = -1;
  }

  toString() {
    return this._buffer.join("");
  }
}
