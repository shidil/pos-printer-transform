import "mocha";
import { expect } from "chai";
import StringBuilder from "./index";

describe("StringBuilder", () => {

  describe("constructor()", () => {

    it("should create a new empty StringBuilder-object", () => {

      // ARRANGE: Nothing to do here

      // ACT
      const stringBuilder = new StringBuilder();

      // ASSERT
      expect(stringBuilder.length).to.eq(0);
      expect(stringBuilder.toString()).to.eq("");
    });
  });

  describe("constructor(newLine)", () => {

    it("should create a new empty StringBuilder-object (custom newLine", () => {

      // ARRANGE
      const newLine = "ABC";

      // ACT
      const stringBuilder = new StringBuilder(newLine);

      // ASSERT
      expect(stringBuilder.length).to.eq(0);
      expect(stringBuilder.toString()).to.eq("");
    });
  });

  describe("#append(text)", () => {

    it("should add the strings", () => {

      // ARRANGE
      const stringBuilder = new StringBuilder();
      const string1 = "ABC";
      const string2 = "XYZ";

      // ACT
      stringBuilder.append(string1);
      stringBuilder.append(string2);

      // ASSERT
      expect(stringBuilder.toString()).to.eq(`${string1}${string2}`);
    });

    it("should ignore null", () => {

      // ARRANGE
      const stringBuilder = new StringBuilder();
      const string1 = "ABC";
      const string2 = undefined;
      const string3 = "XYZ";

      // ACT
      stringBuilder.append(string1);
      stringBuilder.append(string2);
      stringBuilder.append(string3);

      // ASSERT
      expect(stringBuilder.toString()).to.eq(`${string1}${string3}`);
    });
  });

  describe("#appendLine(text)", () => {

    it("should add the lines", () => {

      // ARRANGE
      const stringBuilder = new StringBuilder();
      const string1 = "ABC";
      const string2 = "XYZ";

      // ACT
      stringBuilder.appendLine(string1);
      stringBuilder.appendLine(string2);

      // ASSERT
      expect(stringBuilder.toString()).to.eq(`${string1}${stringBuilder._newLine}${string2}${stringBuilder._newLine}`);
    });

    it("should ignore null", () => {

      // ARRANGE
      const stringBuilder = new StringBuilder();
      const string1 = "ABC";
      const string2 = undefined;
      const string3 = "XYZ";

      // ACT
      stringBuilder.appendLine(string1);
      stringBuilder.appendLine(string2);
      stringBuilder.appendLine(string3);

      // ASSERT
      expect(stringBuilder.toString()).to.eq(`${string1}${stringBuilder._newLine}${string3}${stringBuilder._newLine}`);
    });
  });

  describe("#appendLine(text)", () => {

    it("should add the lines", () => {

      // ARRANGE
      const stringBuilder = new StringBuilder();
      const string1 = "ABC";
      const string2 = "XYZ";

      // ACT
      stringBuilder.appendLine(string1);
      stringBuilder.appendLine(string2);

      // ASSERT
      expect(stringBuilder.toString()).to.eq(`${string1}${stringBuilder._newLine}${string2}${stringBuilder._newLine}`);
    });

    it("should ignore null", () => {

      // ARRANGE
      const stringBuilder = new StringBuilder();
      const string1 = "ABC";
      const string2 = undefined;
      const string3 = "XYZ";

      // ACT
      stringBuilder.appendLine(string1);
      stringBuilder.appendLine(string2);
      stringBuilder.appendLine(string3);

      // ASSERT
      expect(stringBuilder.toString()).to.eq(`${string1}${stringBuilder._newLine}${string3}${stringBuilder._newLine}`);
    });
  });

  describe("#get length()", () => {

    it("should return the length", () => {

      // ARRANGE
      const stringBuilder = new StringBuilder();
      const string1 = "ABC";
      const string2 = "XYZ";
      const expectedLength = string1.length + string2.length;
      stringBuilder.append(string1);
      stringBuilder.append(string2);

      // ACT
      const length = stringBuilder.length;

      // ASSERT
      expect(length).to.eq(expectedLength);
    });

    it("should return the length - newLine", () => {

      // ARRANGE
      const stringBuilder = new StringBuilder();
      const string1 = "ABC";
      const string2 = "XYZ";
      const expectedLength = string1.length + stringBuilder._newLine.length + string2.length + stringBuilder._newLine.length;
      stringBuilder.appendLine(string1);
      stringBuilder.appendLine(string2);

      // ACT
      const length = stringBuilder.length;

      // ASSERT
      expect(length).to.eq(expectedLength);
    });
  });

  describe("#clear()", () => {

    it("should clear the buffer", () => {

      // ARRANGE
      const stringBuilder = new StringBuilder();
      const string1 = "ABC";
      const string2 = "XYZ";
      stringBuilder.appendLine(string1);
      stringBuilder.appendLine(string2);

      // ACT
      stringBuilder.clear();

      // ASSERT
      expect(stringBuilder.toString().length).to.eq(0);
      expect(stringBuilder.length).to.eq(0);
    });
  });
});
