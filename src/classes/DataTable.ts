import { MessageStyle } from "../enums/MessageStyle";
import { MessageAlignment } from "../enums/MessageAlignment";

export default class DataTable {
  public header: [string, string, string];
  public rows: Array<[string, string, string]>;

  public styleHeader: MessageStyle = MessageStyle.Normal;

  public alignmentColumn1: MessageAlignment = MessageAlignment.Left;
  public alignmentColumn2: MessageAlignment = MessageAlignment.Left;
  public alignmentColumn3: MessageAlignment = MessageAlignment.Left;

  public styleColumn1: MessageStyle = MessageStyle.Normal;
  public styleColumn2?: MessageStyle = MessageStyle.Normal;
  public styleColumn3?: MessageStyle = MessageStyle.Normal;

  public widthColumn1: number = 0;
  public widthColumn2: number = 0;
  public widthColumn3: number = 0;
}
