import * as moment from "moment";
import Order from "../../interfaces/Order";
import OrderItem from "../../interfaces/OrderItem";
import Renderer from "../../interfaces/Renderer";
import PrinterStream from "../PrinterStream/index";
import { MessageStyle } from "../../enums/MessageStyle";
import { MessageAlignment } from "../../enums/MessageAlignment";
import { KotItem } from "../../interfaces/OrderItem";

export default class KOTRenderer implements Renderer {
  stream: PrinterStream;

  constructor() {
    this.stream = new PrinterStream();
  }

  public render(order: Order): string {
    const isCancelled: boolean = order.orderStatus === 4 || order.orderStatus === "Cancelled";

    this.renderHeader(order);
    (isCancelled ? this.renderCancelledOrderItems : this.renderOrderItems)(order.orderItems);
    this.renderFooter(order);

    const result = this.stream.getResult();

    this.stream.flush();

    return result;
  }

  private renderHeader(order: Order) {
    const hasTable: boolean = !!order.table;
    const guestCount: number = order.guestCount;
    const orderNumber: string = order.orderNumber;
    const orderId = order.id && order.id.length > 10;
    const orderTypeName: string = order.orderType.name;
    const businessName: string = order.header.businessName;
    const dateFormatted: string = moment().format("DD/MM/YYYY");
    const timeFormatted: string = moment().format("hh:mm:ss A");
    const tableCode: string = hasTable ? order.table.code : undefined;
    const staffName: string = order.orderPlacementDetail.staffName.toUpperCase();
    const registerName: string = order.orderPlacementDetail.registerName.toUpperCase();
    const isCancelled: boolean = order.orderStatus === 4 || order.orderStatus === "Cancelled";
    this.stream.appendLines(MessageStyle.AlignCenter, businessName);
    this.stream.appendEmptyLines();

    // START FIRSTLINE

    this.stream.append(MessageStyle.Normal, "Order # ");
    this.stream.appendWithAlignment(MessageStyle.Bold, MessageAlignment.Left, orderNumber, 14);

    this.stream.appendWithAlignment(
      MessageStyle.Bold,
      MessageAlignment.Left,
      `${dateFormatted} ${timeFormatted}`, 20);

    this.stream.appendEmptyLines();

    // END FIRSTLINE

    // START SECOND LINE

    this.stream.append(MessageStyle.Normal, "by ");
    this.stream.append(MessageStyle.Underline, staffName);

    if (registerName !== null || registerName !== "") {
      this.stream.append(MessageStyle.Normal, "    from ");
      this.stream.append(MessageStyle.Underline, registerName);
    }

    this.stream.appendEmptyLines();
    this.stream.appendEmptyLines();

    // END SECOND LINE

    // START THIRD LINE

    if (isCancelled) {
      this.stream.appendLines(MessageStyle.AlignCenter | MessageStyle.SizeDouble | MessageStyle.Bold, "CANCELLED\n");
    }

    this.stream.append(MessageStyle.Bold, orderTypeName);

    if (orderTypeName.toLowerCase() === "dine in" && hasTable) {
      this.stream.appendSpaces(3);
      this.stream.append(MessageStyle.Bold, tableCode.toUpperCase());
    }

    if (!isCancelled) {
      this.stream.appendSpaces(3);
      this.stream.append(MessageStyle.Bold, orderId ? "EDITED" : "NEW ORDER");

      if (hasTable) {
        this.stream.appendSpaces(3);
        this.stream.append(MessageStyle.Bold | MessageStyle.AlignRight, guestCount);
        this.stream.append(MessageStyle.Normal, " Guests");
      }
    }

    this.stream.appendEmptyLines();

    // endregion

    this.stream.appendHorizontalLine();
  }

  private renderFooter(order: Order) {
    const kotCount: number = order.kotCount;
    const kotLogs: Array<KotItem> = order.kotLogs;
    const notes: Array<KotItem> = kotLogs.filter(x => !x.isPrinted);

    this.stream.appendHorizontalLine();
    notes.forEach(note => {
      if (note.notes !== null && note.notes !== "") {
        this.stream.appendLines(MessageStyle.Normal, `Order Notes: <b>${note.notes}</b>`);
      }
    });

    this.stream.appendLines(MessageStyle.AlignCenter, `<b>KOT #: ${kotCount}</b>`);
    this.stream.appendEmptyLines(5);
  }

  private _isCancelled = (status: string | number) => {
    return status === "Cancelled" || status === 3;
  }

  private renderOrderItems = (orderItems: Array<OrderItem>) => {
    let lastcategoryName: string = "";

    let WidthColumn1: number = 10;
    let WidthColumn2: number = 5;
    let WidthColumn3: number = 28;

    orderItems.forEach(orderItem => {
      const kotItem = orderItem.kotLog.pop() || undefined;

      if (!kotItem) {
        return;
      }

      if (!kotItem.isPrinted) {
        if (lastcategoryName !== orderItem.product.categoryName) {
          lastcategoryName = orderItem.product.categoryName;
          this.stream.appendLines(MessageStyle.Underline, orderItem.product.categoryName || "undefined");
          this.stream.appendEmptyLines(1);
        }

        if (kotItem.isDeleted) {
          this.stream.appendLines(MessageStyle.AlignLeft | MessageStyle.Normal, "CANCELLED ITEM");
        }

        this.stream.appendTuppleLine(MessageStyle.Normal,
          [MessageAlignment.Right, MessageAlignment.Center, MessageAlignment.Left],
          [kotItem.qty === 0 ? "No Qty Change" : kotItem.qty + "", "x", kotItem.name],
          [WidthColumn1, WidthColumn2, WidthColumn3]);

        if (kotItem.modifierKotLog) {
          kotItem.modifierKotLog.forEach(modifier => {
            this.stream.appendTuppleLine(
              MessageStyle.Normal,
              [MessageAlignment.Right, MessageAlignment.Left],
              [modifier.isDeleted ? "-" : "+", `${modifier.qty} * ${modifier.name}`],
              [WidthColumn1 + WidthColumn2, WidthColumn3]);
            this.stream.appendEmptyLines(1);
          });
        }

        if (kotItem.notes) {
          this.stream.appendTuppleLine(MessageStyle.Bold,
            [MessageAlignment.Right, MessageAlignment.Left, MessageAlignment.Left],
            ["Item notes", "", `[${kotItem.notes}]`],
            [WidthColumn1 + WidthColumn2 - 2, 2, WidthColumn3]);
          this.stream.appendEmptyLines(1);
        }
      }
      kotItem.isPrinted = true;
    });
  }

  private renderCancelledOrderItems = (orderItems: Array<OrderItem>) => {
    let WidthColumn1: number = 10;
    let WidthColumn2: number = 5;
    let WidthColumn3: number = 28;

    this.stream.appendLines(MessageStyle.AlignLeft | MessageStyle.Bold, "CANCELLED ITEMS\n");
    this.stream.appendEmptyLines(1);

    orderItems.filter(x => this._isCancelled(x.orderItemStatus)).forEach(orderItem => {
        const kotItem = orderItem.kotLog.pop() || <KotItem>{};

        if (!kotItem.isPrinted) {
          this.stream.appendTuppleLine(
            MessageStyle.Normal,
            [MessageAlignment.Right, MessageAlignment.Center, MessageAlignment.Left],
            [kotItem.qty + "", "x", kotItem.name],
            [WidthColumn1, WidthColumn2, WidthColumn3]);

          if (kotItem.modifierKotLog) {
            kotItem.modifierKotLog.forEach(modifier => {
              this.stream.appendTuppleLine(
                MessageStyle.Normal,
                [MessageAlignment.Right, MessageAlignment.Left],
                [modifier.isDeleted ? "-" : "+", `  + ${modifier.qty} * ${modifier.name}`],
                [WidthColumn1 + WidthColumn2, WidthColumn3]);
              this.stream.appendEmptyLines(1);
            });
          }

          if (kotItem.notes) {
            this.stream.appendTuppleLine(
              MessageStyle.Bold,
              [MessageAlignment.Right, MessageAlignment.Left, MessageAlignment.Left],
              ["Item notes", "", `[${kotItem.notes}]`],
              [WidthColumn1 + WidthColumn2 - 2, 2, WidthColumn3]);
            this.stream.appendEmptyLines(1);
          }
        }

        kotItem.isPrinted = true;
    });
  }
}
