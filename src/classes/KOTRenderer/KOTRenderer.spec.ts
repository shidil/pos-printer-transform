import "mocha";
import * as order from "../../order.json";
import { expect } from "chai";
import KOTRenderer from "./index";

describe("KOTRenderer", () => {

  describe("constructor()", () => {

    it("should create a new empty KOTRenderer-object", () => {

      // ARRANGE

      // ACT
      const renderer = new KOTRenderer();
      const result = renderer.render(<any>order);

      console.log(result);

      // ASSERT
      expect(renderer).to.be.an("object");
      expect(result).to.be.a("string");
    });
  });
});
