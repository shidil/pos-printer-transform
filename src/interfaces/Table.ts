export default interface Table {
  name: string;
  code: string;
  seatingCapacity: number;
  height: number;
  width: number;
  left: number;
  top: number;
  tableType: "Table" | "Text" | "Structure";
  layoutId: string;
  layoutName: string;
  staffId: string;
  type: string;
  auditLogs: Array<any>;
  isDeleted: boolean;
  id: string;
}
