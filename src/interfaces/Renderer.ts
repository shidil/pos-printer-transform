import PrinterStream from "../classes/PrinterStream/index";

export default interface Renderer {
  stream: PrinterStream;
  render: Function;
}
