import Customer from "./Customer";
import Table from "./Table";
import OrderItem from "./OrderItem";
import { KotItem } from "./OrderItem";

export interface OrderPlacementDetail {
  time: string;
  shiftId: string;
  staffId: string;
  staffName: string;
  registerId: string;
  deviceId: string;
  registerName: string;
}

export interface OrderType {
  code: string;
  requireCustomerIdentifier: boolean;
  orderChannel: "StoreFront" | "Online";
  tax: string;
  fee: string;
  tenantId: string;
  type: string;
  auditLogs: Array<any>;
  isDeleted: boolean;
  id: string;
  name: string;
}

export interface Discount {
  discountType: "Percentage" | "Amount";
  value: number;
  amount: number;
}

export interface OrderTax {
  id: string;
  name: string;
  amount: number;
  rate: number;
  taxType: "Tax" | "GroupTax" | "Fee" | "GroupFee";
  isInclusive: boolean;
}

export interface OrderPayment {
  amount: number;
  paymentChannel: "General" | "Integrated" | "Customer";
  id: string;
  name: string;
}

export interface OrderAction {
  action: "PlacedOrder" | "EditedOrder" | "AddedItem" | "RemovedItem" | "IncreasedItemQuantity"
          | "DecreasedItemQuantity" | "AddedModifierToItem" | "RemovedModifierFromItem" | "AddedDiscount" |
          "RemovedDiscount" | "SetOrderToCancel" | "SetOrderToPaid" | "AddedCustomer" | "RemovedCustomer" | "VoidedOrder" | "Reprint";
  staffId: string;
  staffName: string;
  deviceId: string;
  registerId: string;
  registerName: string;
  timestamp: string;
  notes: string;
}

export interface Header {
  businessName: string;
  businessId: string;
  address: string;
  phone: string;
}

export default interface Order {
  id?: string;
  header?: Header;
  orderPlacementDetail: OrderPlacementDetail;
  orderType: OrderType;
  endTime: string;
  startTime: string;
  discount: Discount;
  isPaid: boolean;
  subtotal: number;
  total: number;
  orderNumber: string;
  notes: string;
  bumpStatus: "Inprogress" | "Done" | "Cancelled";
  orderStatus: "Doe" | "InProgress" | "Started" | "Edited" | "Cancelled" | "Refund" | "Printed" | "Voided" | 0 | 4;
  orderActions: Array<OrderAction>;
  customer: Customer;
  orderTaxes: Array<OrderTax>;
  orderPayments: Array<OrderPayment>;
  orderItems: Array<OrderItem>;
  table: Table;
  guestCount: number;
  kotLogs: Array<KotItem>;
  kotCount: number;
  changeDue?: number;
}
