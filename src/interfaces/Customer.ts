export interface WebLink {
  name: string;
  value: string;
}

export interface Celebration {
  name: string;
  value: string;
}

export default interface Customer {
  phone: string;
  address: string;
  locale: string;
  email: string;
  company: string;
  webLinks: Array<WebLink>;
  celebrations: Array<Celebration>;
  gender: string;
  createdOn: string;
  balance: number;
  outlets: Array<any>;
  balanceHistory: Array<any>;
  tenantId: string;
  type: string;
  auditLogs: Array<Object>;
  isDeleted: boolean;
  id: string;
  name: string;
}
