export default interface UnitOfMeasure {
  unitMeasure: "Qty" | "Gm" | "Kg" | "L" | "Ml";
  measureType: "NonWeighted" | "SolidWeightedLower" | "SolidWeightedHigher" | "LiquidWeightedHigher" | "LiquidWeightedLower";
}
