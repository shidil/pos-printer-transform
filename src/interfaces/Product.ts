import UnitOfMeasure from "./UnitOfMeasure";

export interface ProductPricing {
  sellingPrice: number;
  markup: number;
  priceBeforeTax: number;
  costPrice: number;
  taxId: string;
  outletId: string;
  isActive: boolean;
  orderTypes: Array<string>;
  tenantId: string;
  type: string;
  auditLogs: Array<any>;
  isDeleted: boolean;
  id: string;
  name: string;
}

export interface ProductModifier {
  quantity: number;
  unitofmeasure: UnitOfMeasure;
  isDisplayed: boolean;
  isMapped: boolean;
  isAllowed: boolean;
  isAdded: boolean;
  isDeleted: boolean;
  id: string;
  name: string;
  price: number;
}

export default interface Product {
  subCategoryId: string;
  subCategoryName: string;
  categoryId: string;
  categoryName: string;
  productDescription: string;
  isActive: boolean;
  displayOrder: number;
  isModifier: boolean;
  barcode: string;
  efficiencyFactor: number;
  isTemplate: boolean;
  isVariantProduct: boolean;
  averagePrepTime: string;
  unitOfMeasure: UnitOfMeasure;
  modifiers: Array<ProductModifier>;
  productPricings: Array<ProductPricing>;
  outlets: Array<any>;
  variants: Array<any>;
  productInventories: Array<any>;
  categories: Array<any>;
  brand: any;
  tags: Array<string>;
  supplierDetails: Array<any>;
  variantProds: Array<any>;
  parentProduct: string;
  isComposite: boolean;
  tenantId: string;
  type: string;
  auditLogs: Array<Object>;
  isDeleted: boolean;
  id: string;
  name: string;
}
