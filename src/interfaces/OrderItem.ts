import { OrderTax, Discount } from "./Order";
import Product, { ProductModifier } from "./Product";

export interface KotItem {
  isPrinted: boolean;
  isDeleted: boolean;
  name: string;
  qty: number;
  notes: string;
  modifierKotLog: Array<KotItem>;
}

export default interface OrderItem {
  description: string;
  quantity: number;
  priceBeforeTax: number;
  costPrice: number;
  price: number;
  total: number;
  tax: number;
  discount: number;
  product: Product;
  variantProduct: Product;
  notes: string;
  bumpStatus: "InProgress" | "Done" | "Cancelled";
  averagePrepTime: string;
  orderItemStatus: "New" | "Edited" | "Cancelled" | "Refund";
  orderItemTaxes: Array<OrderTax>;
  orderItemDiscount: Discount;
  modifiers: Array<ProductModifier>;
  kotLog: Array<KotItem>;
}
