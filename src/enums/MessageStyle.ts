export enum MessageStyle {
  Normal = 0,
  Bold = 1 << 1,
  Underline = 1 << 2,
  DoubleStrike = 1 << 3,

  AlignLeft = 1 << 11,
  AlignCenter = 1 << 12,
  AlignRight = 1 << 13,
  Justification = 1 << 14,

  SizeDouble = 1 << 21,
}
