import { MessageStyle } from "../enums/MessageStyle";
import { MessageAlignment } from "../enums/MessageAlignment";

export const NUL = String.fromCharCode(0);

/// Escape
export const ESC = String.fromCharCode(27);

/// Line Feed
export const LF = String.fromCharCode(10);

/// Group Separator
export const GS = String.fromCharCode(29);

export const CMD_INITLIAZE_PRINTER = ESC + "@";

export const ALIGN_LEFT = ESC + "a" + String.fromCharCode(0);
export const ALIGN_CENTER = ESC + "a" + String.fromCharCode(1);
export const ALIGN_RIGHT = ESC + "a" + String.fromCharCode(2);

export const COMMANDS = {
  [MessageStyle.Underline]: "-",
  [MessageStyle.Bold]: "E",
  [MessageStyle.DoubleStrike]: "F",
  [MessageStyle.Justification]: "A",
};

export const COMMAND_ON_SUFFIX = "\u0001";
export const COMMAND_OFF_SUFFIX = "\0";

export const BOLD_ON = ESC + "E" + "\u0001";
export const BOLD_OFF = ESC + "E" + "\0";

export const LINE_FEED = ESC + "d" + String.fromCharCode(1);
export const FONT_B = ESC + "M" + String.fromCharCode(2);

export const DOUBLE_ON = GS + "!" + "\u0011";
export const DOUBLE_OFF = GS + "!" + "\0";

export const CMD_CUT = `${ESC}@${GS}V{1}`;
export const CMD_DRAWER = ESC + "@" + String.fromCharCode(27) + "p" + String.fromCharCode(0) + ".}";
export const CMD_OPEN_DRAWER = ESC + "p" + String.fromCharCode(1);

export const COUNT_CHAR_IN_HR_LINE = 42;

export const DEFAULT_ALIGNEMNT: MessageAlignment = MessageAlignment.Left;
